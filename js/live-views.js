(function($, Drupal) {
  console.log('working');
  var serverSideChannel = new ServerSideChannel('/live-views-channel','');
  serverSideChannel.eventSource.onmessage = function(message) {
    var keys = Object.keys(drupalSettings.live_views);
    if(keys.indexOf(message.data) !== -1){
      for (const [viewId, displays] of Object.entries(drupalSettings.live_views[message.data])) {
        for (const [displayId, displayName] of Object.entries(displays)) {
          console.log(viewId , displayId);
          var viewSelector = ".view-id-"+viewId+'.view-display-id-' + displayId
          $(viewSelector).triggerHandler('RefreshView');
        }
      }
    }
  }
})(jQuery, Drupal)
