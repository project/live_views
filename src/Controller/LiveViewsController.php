<?php

namespace Drupal\live_views\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Live views routes.
 */
class LiveViewsController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
