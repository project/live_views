<?php

/**
 * @file
 * Contains \Drupal\views_head_metadata\Plugin\views\display_extender\HeadMetadata.
 */

namespace Drupal\live_views\Plugin\views\display_extender;

use Drupal\views\Plugin\views\display_extender\DisplayExtenderPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Head metadata display extender plugin.
 *
 * @ingroup views_display_extender_plugins
 *
 * @ViewsDisplayExtender(
 *   id = "live_views",
 *   title = @Translation("head metadata display extender"),
 *   help = @Translation("Settings to add metatag in document head for this view."),
 *   no_ui = FALSE
 * )
 */
class LiveViewsDisplayExtender extends DisplayExtenderPluginBase {

  /**
   * Provide the key options for this plugin.
   */
  public function defineOptions() {
    $options['live_views'] = ['default' => 0];
    return $options;
  }

  /**
   * Provide the default summary for options and category in the views UI.
   */
  public function optionsSummary(&$categories, &$options) {
    if($this->options['live_views'] == 1){
      $liveViews = $this->t('Enabled');
    }
    else{
      $liveViews = $this->t('Disabled');

    }
    $options['live_views']=[
      'category' => 'other',
      'title' => $this->t('Live views'),
      'value' => $liveViews
    ];
  }

  /**
   * Provide a form to edit options for this plugin.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    if ($form_state->get('section') == 'live_views') {
      $form['#title'] .= t('Live views');
      $enabled = $this->getLiveViewsValue();
      $form['live_views'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Live'),
        '#default_value' => $enabled,
      ];
    }
  }

  /**
   * Validate the options form.
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state) { }

  /**
   * Handle any special handling on the validate form.
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    if ($form_state->get('section') == 'live_views') {
      $this->options['live_views'] = $form_state->getValue('live_views');
    }
  }

  /**
   * Set up any variables on the view prior to execution.
   */
  public function preExecute() { }

  /**
   * Inject anything into the query that the display_extender handler needs.
   */
  public function query() { }

  /**
   * Static member function to list which sections are defaultable
   * and what items each section contains.
   */
  public function defaultableSections(&$sections, $section = NULL) { }



  /**
   * Get the head metadata configuration for this display.
   *
   * @return array
   *   The head metadata values.
   */
  public function getLiveViewsValue() {
    $liveViews = $this->options['live_views'];

    return $liveViews;
  }
}


